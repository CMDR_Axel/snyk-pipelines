# Bitbucket Pipelines and Snyk integration

By integrating Snyk into [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines), you can continuously monitor your codebase for known vulnerabilities in your dependencies.

This repo demos how to add Snyk to Bitbucket, with the `bitbucket-pipelines.yml` file that shows the configuration we recommend. 

To find out more about Snyk, head over to [our website](https://snyk.io).

### Creating a policy file

1. Install the Snyk utility using `npm install -g snyk`.
2. Run `snyk wizard` in the directory of your project following the prompts which will also generate a `.snyk` policy file. For more information about this, see [our CLI documentation](/docs/using-snyk/#wizard).
3. When asked by `snyk wizard`, select to include `snyk test` as part of your `npm test` command. If there are new vulnerabilities in the future, Bitbucket Pipelines will fail.
4. Ensure the `.snyk` file you generated was added to your source control (`git add .snyk`).

### Configure Bitbucket Pipelines

After running the steps above to create a `.snyk `policy file, you now need to create a `bitbucket-pipelines.yml` file which will configure Bitbucket Pipelines.

If you aren't using Pipelines yet, go to Bitbucket, add the Bitbucket Pipelines add-on, and enable it for the repository in question. 

### Creating a new bitbucket-pipelines.yml file

Use the `bitbucket-pipelines.yml` from this repo. 

This file has the configuration we recommend to stay secure:

* `snyk protect` applies any patches you chose during `snyk wizard`. 
* `snyk test` will automatically run as part of `npm test` and will fail the pipeline if any known vulnerabilities are found in the dependencies.
* Snyk alerts you when newly disclosed vulnerabilities affect your project's dependencies. `snyk monitor` makes sure we have an up-to-date list of your dependencies to do so accurately. You'll also need to authenticate to Snyk, so we know where to update the dependencies.

As part of setup, you need to configure your environment to include the `SNYK_TOKEN` environment variable in the Bitbucket settings. You can find your API token in your [account settings on snyk.io](https://snyk.io/account/). 

![Configuring the environment variable](http://res.cloudinary.com/snyk/image/upload/c_scale,w_500/v1475078005/Configure_env_var_on_BB.png)

*Configuring the environment variable*

### Adapting an existing bitbucket-pipelines.yml file

If you want to adapt an existing `bitbucket-pipelines.yml` file, we recommend to add Snyk like this:

```yaml
- npm install
# authenticate with snyk
- node node_modules/snyk/cli auth ${SNYK_TOKEN} -d
# run snyk protect to apply any patches
- node node_modules/snyk/cli protect
# snyk test will run as part of npm test and fail if it finds vulnerabilities
- npm test
# snyk monitor updates the dependencies Snyk will monitor for new vulnerabilities
- if [ $BITBUCKET_BRANCH == "master" ]; then node node_modules/snyk/cli monitor; fi; 
```
You'll need to configure your environment to include the `SNYK_TOKEN` environment variable. You can find your API token in your [account settings on snyk.io](https://snyk.io/account/). 

### Enable Pipelines

You should now enable Pipelines. Bitbucket Pipelines will fail if any known vulnerabilities are found in your dependencies.